![h3xOs Logo](https://i.postimg.cc/Y2fPGyNZ/h3xos.png)


Introduction
=======
h3xOs is an open source modular kernel and operating system written in Assembly language. Why you might ask, to limit many
of the complications that occur when developers of high level languages compile software without considering the how the compiler
and linker will have on the resulting Assembly and binary code.

h3xOS is designed to be the most secure operating system in terms of possible segmentation faults and vulnerabilities with the OS layer itself.
Adding 3rd party software to h3xOS will of course open it up to vulnerabilities at higher levels but the plan is to make the OS level very secure.

Developer
=======
James Vince of h3xtech.com

Development Environment
=======
```nasm
TargetOS : 'Linux', 'Windows', 'MAC OSX'
ProgramLanguage : Assembly(NASM)
Develop Env tools : nasm, vim, qemu 2.3.x
```
- The h3xOS project is not made to the C/C++ language, and it has made to only the Netwide Assembler and Makefile.
- The Source of this project is possible to compile in Linux and windows.

Install to MAC OSX
======
1. Install ```Git```
2. git clone https://kooblico@bitbucket.org/h3xtech/h3xos.git
3. insert to USB in your PC<br />
4. Install ```NASM```
5. Move to h3xOS in your project directory: ```cd ~/Github/h3xOS/hello```
6. Install h3xOS in your USB: ```make DRIVE_NAME=YOUR_USB_NAME```<br />
![2015-08-31 11 46 39](https://cloud.githubusercontent.com/assets/7445459/9581910/226ec6de-503d-11e5-9632-7a2656788c56.png)
7. Execute h3xOS: ```make DRIVE_NAME=YOUR_USB_NAME run```<br />
![2015-08-31 11 46 57](https://cloud.githubusercontent.com/assets/7445459/9581913/26c993c6-503d-11e5-8894-70c0220c1679.png)

About
=======
- The License of this project is GNU General Public License (GPL) 2.0	.
- This is the GUI Operating System that you can run from or install from USB.
- This project has be created to help people understand the underlying factors that affect all operating systems written 3rd level languages
- h3xOs documentation had be created in English, Japanese and I'm now working on an Italian translation

This project's means to me
=====
- For years I have wanted to create an operating system based on Linux without some of the high level issues that 3rd level language OSes have
- Helping people understand how an operating system works, how memory is moved between registers and how binary code delivers commands
- The goal is to develop an arm version of h3xOS so people can learn the differences between the i386 and RISC command sets
- I love explaining to people how tech works on a deep level and a video can be found on h3xtech.com explaining how h3xOS works
