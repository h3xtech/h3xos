![h3xOs Logo](https://i.postimg.cc/Y2fPGyNZ/h3xos.png)


# h3xOS V1.0

1. You will need a *nix environment to install h3xOS
	* Windows user can install `cygwin` : https://cygwin.com/install.html
2. You will need to install the following dependencies to compile the code
	* A GCC comparable C++ compiler
	* LibCDirective
	* LibCurl
